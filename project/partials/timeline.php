        <div class="timeline-wrapper">
        <div class="timeline-cover"></div>

            <div class="col-xs-2 nopad">
                <div class="timeline-control-button-container">
                    <div class="col-xs-4 nopad">
                        <div id="jsSkipBackButton" class="timeline-control-button timeline-control-button__back" title="Skip back"><span class="sr-only">Skip back</span></div>
                    </div>
                    <div class="col-xs-4 nopad">
                        <div id="jsPlayPauseButton" class="timeline-control-button timeline-control-button__play-pause play" title="Play/Pause"><span id="jsPlayPauseSRText" class="sr-only">Play/Pause</span></div>
                    </div>
                    <div class="col-xs-4 nopad">
                        <div id="jsSkipForwardButton" class="timeline-control-button timeline-control-button__forward" title="Skip forward"><span class="sr-only">Skip forward</span></div>
                    </div>
                </div>
            </div>

            <div class="col-xs-8 nopad">

                <div id="jsTimelineProgressHover" class="timeline-progress-hover"></div>
                <div id="jsTimelineProgress" class="timeline-progress">
                    <div id="jsTimelineIndicator" class="timeline-indicator"></div>
                </div>

                <div id="jsTimelineContainer" class="col-xs-12 timeline-chapter-buttons nopad">

                    <div class="chapter-button chapter-button__introduction">
                        <!-- This chapter button only has one state associated with it -->
                        <div class="timeline-state" data-percent-width="20" data-percent-start="0" data-state="introduction"></div>
                        Introduction
                    </div>

                    <div class="chapter-button chapter-button__chapter1">
                        <!-- This chapter button has two states associated with it -->
                        <div class="timeline-state" data-percent-width="10" data-percent-start="20" data-state="chapter1"></div>
                        <div class="timeline-state" data-percent-width="10" data-percent-start="30" data-state="chapter1-pt2"></div>
                        Chapter 1
                    </div>

                    <div class="chapter-button chapter-button__chapter2">
                        <div class="timeline-state" data-percent-width="10" data-percent-start="40" data-state="chapter2"></div>
                        Chapter 2
                    </div>

                    <div class="chapter-button chapter-button__chapter3">
                        <div class="timeline-state" data-percent-width="20" data-percent-start="50" data-state="chapter3"></div>
                        Chapter 3
                    </div>

                    <div class="chapter-button chapter-button__chapter4">
                        <div class="timeline-state" data-percent-width="15" data-percent-start="70" data-state="chapter4"></div>
                        Chapter 4
                    </div>

                    <div class="chapter-button chapter-button__thanks">
                        <div class="timeline-state" data-percent-width="15" data-percent-start="85" data-state="thanks"></div>
                        Thanks
                    </div>

                </div>

            </div>

            <div class="col-xs-2 nopad">
                <div class="col-xs-4 nopad">
                    <div id="jsLanguageSelectorContainer" class="timeline-control-language-container">
                        <div id="jsLanguageSelectorButton" title="Select Language"><span class="sr-only">Select Language</span>
                            <div id="jsLanguageSelectorButtonIcon" class="timeline-control-button timeline-control-button__language"></div>
                            <div id="jsLanguageSelectorButtonPopout" class="timeline-control-language__popout">
                                <div class="timeline-control-language__item timeline-control-language__item-top" data-language="en">
                                    <span class="selected">✔</span>
                                    English (EN)
                                </div>
                                <div class="timeline-control-language__item" data-language="es">
                                    Español (ES)
                                </div>
                            </div>
                        </div>
                    </div>
                </div> 
                
                <div class="col-xs-4 nopad">
                    <div id="jsQualitySelectorContainer" class="timeline-control-quality-container">
                        <div id="jsQualitySelectorButton" title="Select Quality"><span class="sr-only">Select Quality</span>
                            <div id="jsQualitySelectorButtonIcon" class="timeline-control-button timeline-control-button__quality sd"></div>
                            <div id="jsQualitySelectorButtonPopout" class="timeline-control-quality__popout">
                                <div class="timeline-control-quality__item timeline-control-quality__item-top" data-quality="1080p">
                                    <span class="auto">•</span>
                                    <span class="selected">✔</span>
                                    1080p
                                </div>
                                <div class="timeline-control-quality__item" data-quality="720p">
                                    <span class="auto">•</span>
                                    <span class="selected">✔</span>
                                    720p
                                </div>
                                <div class="timeline-control-quality__item" data-quality="540p">
                                    <span class="auto">•</span>
                                    <span class="selected">✔</span>
                                    540p
                                </div>
                                <div class="timeline-control-quality__item" data-quality="360p">
                                    <span class="auto">•</span>
                                    <span class="selected">✔</span>
                                    360p
                                </div>
                                <div class="timeline-control-quality__item selected" data-quality="Auto">
                                    <span class="selected">✔</span>
                                    Auto
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-xs-4 nopad">
                    <div id="jsVolumeButtonContainer" class="timeline-control-volume-container">
                        <div id="jsVolumeButton" class="timeline-control-volume" title="Volume"><span id="jsVolumeButtonSRText" class="sr-only">Volume</span>
                            <div id="jsVolumeButtonIcon" class="timeline-control-button timeline-control-button__volume"></div>
                            <div id="jsVolumeButtonPopout" class="timeline-control-volume__popout">
                                <div id="jsVolumeButtonPopoutBg" class="timeline-control-volume__popout-bg"></div>
                                <div id="jsVolumeButtonPopoutBgSelectedVolume" class="timeline-control-volume__popout-bg-selected-volume">
                                    <div id="jsVolumePopoutBall" class="timeline-control-volume__popout-ball"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
