/**
 * All the possible quality settings that must be accounted for
 */
var QualitySettings = {
    AUTO : "Auto",
    LOW : "360p",
    MEDIUM : "540p",
    HIGH : "720p",
    FULL_HD : "1080p"
}

var QualitySelector = {
    /**
     * Initialise the quality selector
     */
    initialise: function(startingQuality) {
        // Events for quality selector
        QualitySelector.events.initialise();

        // Starting value
        if (typeof startingQuality == 'undefined') {
            startingQuality = QualitySettings.AUTO;
        }
        QualitySelector.setQuality(startingQuality);
    },

    /**
     * Set quality with one of the values from QualitySettings
     *
     * @param value
     */
    setQuality: function(value) {
        // TODO: Highlight the current quality setting

        // $('#jsVolumeButtonPopoutBgSelectedVolume').height($('#jsVolumeButtonPopoutBg').height() * value);

        // TODO: Inform the VideoPlayerInterface of the change in quality.
        // e.g. VideoPlayerInterface.actions.qualityChange(value);

        // Set volume logo bars
        if (value == QualitySettings.FULL_HD) {
            // TODO: Show highlighted HD icon
        } else {
            // TODO: Show dimmed HD button
        }
    },

    /**
     * Build the ID of the quality selector button
     */
    getQualityButtonID: function(quality) {
        return "jsQualitySelector" + quality + "Button";
    },

    /**
     * Define the events for the quality selector
     */
    events: {
        /**
         * Link up the events and the event handlers
         */
        initialise: function() {
            $('#jsQualitySelectorButton').click(QualitySelector.events.qualityButtonClickEventHandler);
            $('#jsQualitySelectorContainer').hover(QualitySelector.events.qualityButtonHoverInEventHandler, QualitySelector.events.qualityButtonHoverOutEventHandler);
            $('#jsQualityButtonPopoutBg').click(QualitySelector.events.qualityPopoutClick);
        },

        /**
         * Switch between two quality settings
         */
        qualityButtonClickEventHandler: function(e) {
            // TODO: How should the quality be changed?
            // Between Auto and Full HD?
        },

        /**
         * Display the quality selector control when the user hovers over the quality icon
         */
        qualityButtonHoverInEventHandler: function(e) {
            var button = $('#jsQualitySelectorButton');
            var container = $('#');
            var popout = $('#jsQualitySelectorButtonPopout');
            if (!container.is(e.target) && container.has(e.target).length === 0) {
                if (!popout.hasClass('open')) {
                    // Open quality control
                    popout.addClass('open');
                }
            }
        },

        /**
         * Hide the quality selector control when the user moves their mouse away from the quality icon
         */
        qualityButtonHoverOutEventHandler: function(e) {
            var button = $('#jsQualitySelectorButton');
            var popout = $('#jsQualitySelectorButtonPopout');
            if (popout.hasClass('open')) {
                // Close quality control
                popout.removeClass('open');
            }
        }
    }
}
